﻿using System;

namespace palindrome
{
    class Program
    {
        public Boolean isPalindrome(string str)
        {
            int len = str.Length;
            for (int i = 0; i < len / 2; i++)
            {
                if (str[i] != str[len - 1 - i])
                    return false;
            }
            return true;
        }
        static void Main(string[] args)
        {
            Program program = new Program();

            Console.WriteLine(program.isPalindrome("voov"));
        }
    }
}
